#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	
	DatosMemCompartida* memoriap;
	char* proyeccion;
	//Realizamos la apertura del fichero
	int fdfile;
	fdfile = open("/tmp/Botdata.txt",O_RDWR);
	// proyeccion del fichero
	proyeccion = (char*)mmap(NULL,sizeof(*(memoriap)),PROT_WRITE|PROT_READ,MAP_SHARED,fdfile,0);
	
	close(fdfile);
	
	memoriap=(DatosMemCompartida*)proyeccion;
	
	while(1)
	{
		usleep(25000);  
		float posRaqueta;
		
		posRaqueta=((memoriap->raqueta1.y2+memoriap->raqueta1.y1)/2);
		
		if(posRaqueta<memoriap->esfera.centro.y)
			memoriap->accion=1;
		else if(posRaqueta>memoriap->esfera.centro.y)
			memoriap->accion=-1;
		else
			memoriap->accion=0;
		
	}
	
	munmap(proyeccion,sizeof(*(memoriap)));
}

