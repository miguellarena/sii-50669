#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>

using namespace std;


int main (int argc, char * argv[])
{

	int fd;
	int errores;
	char result[100];
	
	remove("/tmp/logger");

	if (mkfifo("/tmp/logger",0666)!=0)	
		perror("No se pudo crear el fifo");
	fd=open("/tmp/logger", O_RDONLY);
	if (fd==-1) 
		perror("No se pudo abrir el fifo");

	do
	{	
		errores = read(fd, &result, 100*sizeof(char));
		if (errores != (100*sizeof(char)))
		{
			perror("Error al leer el pipe");
			break;
		}
		cout << result << endl;
	
	}while (result[0]!='F');

	if (close(fd))
		perror("No se pudo cerrar el fifo");

	if (unlink("/tmp/logger"))	
		perror("No se pudo desenlazar el fifo");

	return 0;
}

